#!/usr/bin/python

import unittest
import os
import numpy
import json

from songindex import Interface
from importer import GtzanImporter
from featurefactory import AveragedMfccFactory, MelSpectrumFactory, SturmFactory
from utils import split
from songindex2 import FileTable
from songindex2 import DataSource

gtzan_testdir = "/tmp/TestGtzanImporterTempDir"
mjf_testdir = "/tmp/TestMJFTempDir"


def setUpTempFixtureDirectory(base_dir, fixture_dir="fixtures/gtzan_example_folder"):
    fixturedir = os.path.join(os.path.dirname(os.path.realpath(__file__)), fixture_dir)
    try:
        os.system("rm -rf %s" % base_dir)
    except Exception:
        pass
    os.system("cp -r %s %s" % (fixturedir, base_dir))
    return base_dir


class TestSongIndex(unittest.TestCase):
    def setUp(self):
        self.interface = Interface(":memory:")
        self.cursor = self.interface.c

    def setUpTables(self):
        self.interface.create_schema()

    def setUpGenres(self):
        self.setUpTables()
        self.cursor.execute("INSERT INTO genre VALUES (null, 'genre1')")
        self.cursor.execute("INSERT INTO genre VALUES (null, 'genre2')")
        self.cursor.execute("INSERT INTO genre VALUES (null, 'genre3')")

    def setUpSongs(self):
        self.setUpTables()
        features1 = [-517, 91, -331, 696, -1294, 236, -1344, 2346, -1461, 1057, -1322, 643, -1495, 102]
        features2 = [-811, 943, -166, 403, -1203, 2828, -2098, 186, -1451, 1415, -7555, 1345, -841, 7315]
        self.cursor.execute("INSERT INTO song VALUES (null, ?, ?, ?)", ("testfile1.mp3", 1, json.dumps(features1)))
        self.cursor.execute("INSERT INTO song VALUES (null, ?, ?, ?)", ("testfile2.mp3", 3, json.dumps(features2)))

    def test_create_schema(self):
        self.interface.create_schema()
        tables = self.cursor.execute("SELECT name FROM sqlite_master WHERE type='table'")
        self.assertEqual(list(tables), [(u'genre',), (u'song',)])

    def test_insert_genre(self):
        self.setUpTables()
        self.interface.insert_genre("testgenre")
        genres = self.cursor.execute("SELECT name FROM genre")
        self.assertEqual(list(genres), [(u'testgenre',)])

    def test_insert_genre_twice(self):
        self.setUpTables()
        self.interface.insert_genre("testgenre")
        self.interface.insert_genre("testgenre")
        genres = self.cursor.execute("SELECT name FROM genre")
        self.assertEqual(list(genres), [(u'testgenre',)])

    def test_insert_genres(self):
        self.setUpTables()
        self.interface.insert_genres(["testgenre1", "testgenre2", "testgenre3"])
        genres = self.cursor.execute("SELECT name FROM genre")
        self.assertEqual(list(genres), [(u'testgenre1',), (u'testgenre2',), (u'testgenre3',)])

    def test_insert_song(self):
        self.setUpGenres()
        self.interface.insert_song("Your song", 2)
        songs = self.cursor.execute("SELECT * FROM song")
        self.assertEqual(list(songs), [(1, u'Your song', 2, u'null')])

    def test_insert_song_with_genre_name(self):
        self.setUpGenres()
        self.interface.insert_song_with_genre_name("/home/dennis/a-test-song.mp3", "genre2")
        songs = self.cursor.execute("SELECT * FROM song")
        self.assertEqual(list(songs), [(1, u'/home/dennis/a-test-song.mp3', 2, u'null')])

    def test_get_genres(self):
        self.setUpGenres()
        genres = self.interface.get_genres()
        self.assertEqual(genres, ['genre1', 'genre2', 'genre3'])

    def test_get_genre_id(self):
        self.setUpGenres()
        returned_id = self.interface.get_genre_id("genre3")
        self.assertEqual(returned_id, 3)

    def test_get_genre_id_of_nonexistent_genre(self):
        self.setUpGenres()
        returned_id = self.interface.get_genre_id("non-existent-genre")
        self.assertEqual(returned_id, None)

    def test_get_feature_matrix(self):
        self.setUpSongs()
        expected_feature_matrix = numpy.array([
            [-517, 91, -331, 696, -1294, 236, -1344, 2346, -1461, 1057, -1322, 643, -1495, 102],
            [-811, 943, -166, 403, -1203, 2828, -2098, 186, -1451, 1415, -7555, 1345, -841, 7315]
        ])
        feature_matrix = self.interface.get_feature_matrix()
        self.assertEqual(type(feature_matrix), type(numpy.array([])))
        self.assertEqual(feature_matrix.tolist(), expected_feature_matrix.tolist())

    def test_get_genre_vector(self):
        self.setUpSongs()
        expected_genre_vector = numpy.array([[1], [3]])
        genre_vector = self.interface.get_genre_vector()
        self.assertEqual(type(genre_vector), type(numpy.array([])))
        self.assertEqual(genre_vector.tolist(), expected_genre_vector.tolist())

    def test_compute_batch_range(self):
        self.setUpSongs()
        start, end = self.interface.compute_batch_range(0)
        self.assertEqual((start, end), (0, 1))
        self.assertRaises(IndexError, self.interface.compute_batch_range, 33)

    def test_compute_max_batch_id(self):
        self.setUpSongs()
        max_batch_id = self.interface.compute_max_batch_id()
        self.assertEqual(max_batch_id, 0)

    def test_get_feature_matrix_shape(self):
        self.setUpSongs()
        feature_matrix_shape = self.interface.get_feature_matrix_shape()
        self.assertEqual(feature_matrix_shape, self.interface.get_feature_matrix().shape)

    def test_get_number_of_samples(self):
        self.setUpSongs()
        number_of_samples = self.interface.get_number_of_samples()
        self.assertEqual(number_of_samples, 2)

    def test_get_shape_of_a_feature(self):
        self.setUpSongs()
        shape_of_feature = self.interface.get_shape_of_a_feature()
        self.assertEqual(shape_of_feature, (1, 14))

    def test_create_songindex(self):
        self.setUpTables()
        tempdir = setUpTempFixtureDirectory(gtzan_testdir)
        importer = GtzanImporter(tempdir)
        feature_factory = AveragedMfccFactory()
        self.interface.create_songindex(importer, feature_factory)

        genres = self.interface.get_genres()
        self.assertEqual(genres, ['hiphop', 'metal'])

        songs = self.interface.get_filenames()
        expected_songs = [
            (os.path.join(tempdir, "hiphop/hiphop.00014.au")),
            (os.path.join(tempdir, "hiphop/hiphop.00015.au")),
            (os.path.join(tempdir, "hiphop/hiphop.00016.au")),
            (os.path.join(tempdir, "metal/metal.00005.au")),
        ]
        self.assertEqual(songs, expected_songs)

    def test_get_song_names(self):
        self.setUpSongs()
        expected_songnames = ['testfile1.mp3', 'testfile2.mp3']
        songsnames = self.interface.get_filenames()
        self.assertEqual(songsnames, expected_songnames)


class TestGtzanImporter(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(gtzan_testdir)
        self.importer = GtzanImporter(self.tempdir)

    def tearDown(self):
        os.system("rm -rf %s" % self.tempdir)

    def test_get_genres(self):
        genres = self.importer.get_genres()
        self.assertEqual(genres, ['hiphop', 'metal'])

    def test_get_genre_directories(self):
        genre_directories = self.importer.get_genre_directories()
        expected_genre_directories = [
            os.path.join(self.tempdir, "hiphop"),
            os.path.join(self.tempdir, "metal"),
        ]
        self.assertEqual(genre_directories, expected_genre_directories)

    def test_get_song_genre_list(self):
        song_genre_list = self.importer.get_song_genre_list()
        expected_song_genre_list = [
            (os.path.join(self.tempdir, "hiphop/hiphop.00014.au"), "hiphop"),
            (os.path.join(self.tempdir, "hiphop/hiphop.00015.au"), "hiphop"),
            (os.path.join(self.tempdir, "hiphop/hiphop.00016.au"), "hiphop"),
            (os.path.join(self.tempdir, "metal/metal.00005.au"), "metal"),
        ]
        self.assertEqual(song_genre_list, expected_song_genre_list)


class TestSimpleFeatureFactory(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(gtzan_testdir)
        self.factory = AveragedMfccFactory()

    def test_one_song(self):
        features = self.factory.get_features(os.path.join(self.tempdir, "hiphop/hiphop.00014.au"))
        self.assertEqual(features.dtype, "float")
        self.assertEqual(features.shape, (20,))


class TestMelSpectrumFactory(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(gtzan_testdir)
        self.factory = MelSpectrumFactory()

    def test_one_song(self):
        features = self.factory.get_features(os.path.join(self.tempdir, "hiphop/hiphop.00014.au"))
        self.assertEqual(features.dtype, "float")
        self.assertEqual(features.shape, (259, 128))

class TestSturmFactory(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(gtzan_testdir)
        self.factory = SturmFactory()

    def test_one_song(self):
        features = self.factory.get_features(os.path.join(self.tempdir, "hiphop/hiphop.00014.au"))
        self.assertEqual(type(features), type(numpy.array([1,2,3])))

    def test_low_energy(self):
        y = numpy.array([1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1])
        calculated = self.factory.low_energy(y, 1, 3)
        expected = 3/5
        self.assertEqual(calculated, expected)

class TestUtils(unittest.TestCase):
    def test_split(self):
        a = [1, 2, 3, 4, 5, 6, 7]
        a1, a2 = split(a, 0.5)
        self.assertEqual(a1, [1, 2, 3])
        self.assertEqual(a2, [4, 5, 6, 7])
        b1, b2 = split(a, 1)
        self.assertEqual(b1, a)
        self.assertEqual(b2, [])

    def test_split_another(self):
        a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        a1, a2 = split(a, 0.7)
        self.assertEqual(a1, [1, 2, 3, 4, 5, 6, 7])
        self.assertEqual(a2, [8, 9, 10])

    def test_split_two(self):
        a = [1, 2]
        a1, a2 = split(a, 0.3)
        self.assertEqual(a1, [])
        self.assertEqual(a2, a)

    def test_split_empty_list(self):
        a = []
        a1, a2 = split(a, 0.5)
        self.assertEqual(a1, [])
        self.assertEqual(a2, [])


class TestFileTable(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(mjf_testdir,"fixtures/mjf_example_folder")
        self.file_table = FileTable(self.tempdir)

    def tearDown(self):
        os.system("rm -rf %s" % self.tempdir)

    def test_find_mp3_files(self):
        filelist = self.file_table._find_mp3_files(self.tempdir)
        expected_list = [
            '/tmp/TestMJFTempDir/28MDDA13AUNBD/Alanis Morissette -x- Narcissus -x- The Montreux Jazz Festival Archive -x- 2001 -x- 3 -x- 48389.mp3',
            '/tmp/TestMJFTempDir/50MDDA12AUNBD/Ute Lemper -x- Amsterdam -x- The Montreux Jazz Festival Archive -x- 2000 -x- 11 -x- 47326.mp3',
            "/tmp/TestMJFTempDir/53MDA12UNDBAE/Youssou N'Dour -x- New Africa -x- The Montreux Jazz Festival Archive -x- 2000 -x- 9 -x- new1.mp3"
        ]
        self.assertEqual(filelist, expected_list)

    def test_correct_table(self):
        expected_table = [
            {
                'filename': '/tmp/TestMJFTempDir/28MDDA13AUNBD/Alanis Morissette -x- Narcissus -x- The Montreux Jazz Festival Archive -x- 2001 -x- 3 -x- 48389.mp3',
                'first_segment_id': 0,
                'genre': None,
                'nb_segments': 72
            },
            {
                'filename': '/tmp/TestMJFTempDir/50MDDA12AUNBD/Ute Lemper -x- Amsterdam -x- The Montreux Jazz Festival Archive -x- 2000 -x- 11 -x- 47326.mp3',
                'first_segment_id': 72,
                'genre': None,
                'nb_segments': 63
            },
            {
                'filename': "/tmp/TestMJFTempDir/53MDA12UNDBAE/Youssou N'Dour -x- New Africa -x- The Montreux Jazz Festival Archive -x- 2000 -x- 9 -x- new1.mp3",
                'first_segment_id': 135,
                'genre': None,
                'nb_segments': 59
            }
        ]
        self.assertEqual(self.file_table.file_table, expected_table)

    def test_get(self):
        filename = self.file_table.get(72)["filename"]
        expected_filename1 = "/tmp/TestMJFTempDir/50MDDA12AUNBD/Ute Lemper -x- Amsterdam -x- The Montreux Jazz Festival Archive -x- 2000 -x- 11 -x- 47326.mp3"
        self.assertEqual(filename, expected_filename1)

        filename2 = self.file_table.get(100)["filename"]
        self.assertEqual(filename2, expected_filename1)

        filename3 = self.file_table.get(0)["filename"]
        expected_filename3 = '/tmp/TestMJFTempDir/28MDDA13AUNBD/Alanis Morissette -x- Narcissus -x- The Montreux Jazz Festival Archive -x- 2001 -x- 3 -x- 48389.mp3'
        self.assertEqual(filename3, expected_filename3)

        filename4 = self.file_table.get(169)["filename"]
        self.assertEqual(filename4, "/tmp/TestMJFTempDir/53MDA12UNDBAE/Youssou N'Dour -x- New Africa -x- The Montreux Jazz Festival Archive -x- 2000 -x- 9 -x- new1.mp3")

    def test_get_non_existing(self):
        self.assertRaises(IndexError, self.file_table.get, 194)

    def test_available_genres(self):
        genres = self.file_table.available_genres()
        expected_genres = [None]
        self.assertEqual(genres, expected_genres)


class TestDataSource(unittest.TestCase):
    def setUp(self):
        self.tempdir = setUpTempFixtureDirectory(mjf_testdir,"fixtures/mjf_example_folder")
        self.d = DataSource(self.tempdir, MelSpectrumFactory())

    def tearDown(self):
        os.system("rm -rf %s" % self.tempdir)

    def test_get_number_of_samples(self):
        self.assertEqual(self.d.get_number_of_samples(), 194)

    def test_get_shape_of_a_feature(self):
        shape = self.d.get_shape_of_a_feature()
        self.assertEqual(shape, (259, 128))

    def test_get_number_of_train_samples(self):
        self.assertEqual(self.d.get_number_of_train_samples(), 154)

    def test_compute_max_batch_id(self):
        self.assertEqual(self.d.compute_max_batch_id(), 15)



if __name__ == '__main__':
    unittest.main()
