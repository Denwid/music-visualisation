import librosa
import numpy


class FeatureFactory():
    def __init__(self):
        self.many_features_available = False
        self.duration = 3
        self.offset = 0

    def get_features(self, filename, offset=0):
        pass

    def get_many_features(self, filename, nb_features):
        pass


class AveragedMfccFactory(FeatureFactory):
    def get_features(self, filename):
        y, sr = librosa.load(filename)
        mfcc = librosa.feature.mfcc(y, sr)
        mfcc = numpy.mean(mfcc, 1)
        return mfcc


class SpectrumFactory(FeatureFactory):
    def __init__(self, offset=0, duration=9):
        self.offset = offset
        self.duration = duration


class MelSpectrumFactory(SpectrumFactory):
    def __init__(self, offset=0):
        self.many_features_available = True
        self.duration = 3
        self.offset = 0

    def get_features(self, filename, offset=0):
        y, sr = librosa.load(filename, offset=offset, duration=self.duration)
        mel_spectrum = librosa.feature.melspectrogram(y, sr, n_fft=512, hop_length=256, fmin=60, fmax=16000, n_mels=128)
        return mel_spectrum.transpose()

    def get_many_features(self, filename, nb_features=9):
        features = []
        for i in range(0, nb_features):
            self.offset = i*self.duration
            y, sr = librosa.load(filename, offset=self.offset, duration=self.duration)
            mel_spectrum = librosa.feature.melspectrogram(y, sr, n_fft=512, hop_length=256, fmin=60, fmax=16000, n_mels=128)
            features.append(mel_spectrum.transpose())
        return features


class FourierTransformFactory(SpectrumFactory):
    def get_features(self, filename):
        y, sr = librosa.load(filename, offset=self.offset, duration=self.duration)
        fourier_transform = numpy.abs(librosa.stft(y, n_fft=1024))**2
        return fourier_transform.transpose()


class SturmFactory(FeatureFactory):
    def get_features(self, filename, offset=0, analysis_window_duration=3):
        y, sr = librosa.load(filename, offset=offset)
        step = analysis_window_duration*sr
        features = []
        for i in xrange(0, 9):
            y_segment = y[i*step:(i+1)*step]
            zero_crossing_rate = self.mean_and_var(librosa.feature.zero_crossing_rate(y_segment))
            mfcc = numpy.mean(librosa.feature.mfcc(y_segment, sr, n_mfcc=26), axis=1)
            spectral_rolloff = self.mean_and_var(librosa.feature.spectral_rolloff(y_segment, sr))
            spectral_centroid = self.mean_and_var(librosa.feature.spectral_centroid(y_segment, sr))
            flux = self.mean_and_var(librosa.onset.onset_strength(y, sr))
            spectral_contrast = numpy.mean(librosa.feature.spectral_contrast(y_segment, sr), axis=1)
            low_energy = self.low_energy(y_segment, sr)
            #TODO: msfm
            #TODO: msfc
            #TODO: modulationspectralcontrast
            features.extend(zero_crossing_rate)
            features.extend(mfcc)
            features.extend(spectral_contrast)
            features.extend(spectral_centroid)
            features.extend(flux)
            features.extend(spectral_rolloff)
            features.append(low_energy)
        if len(features) != 378:
            print "Error not enough samples. Only %s." % len(features)
        return numpy.array(features)

    @staticmethod
    def mean_and_var(s):
        mean = numpy.mean(s)
        variance = numpy.var(s)
        return mean, variance

    @staticmethod
    def low_energy(y, sr, frame_length=2048):
        overall_average = numpy.mean(numpy.square(y))

        y_framed = librosa.util.frame(y, frame_length, frame_length)
        nb_frames = y_framed.shape[1]
        average_per_frame = numpy.mean(numpy.square(y_framed), axis=1)

        low_frames = average_per_frame < overall_average

        return sum(low_frames)/nb_frames




