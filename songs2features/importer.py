import os


class Importer():
    def get_genres(self):
        return []

    def get_song_list(self):
        return []

    def get_song_genre_list(self):
        return []


class GtzanImporter(Importer):
    def __init__(self, directory):
        self.directory = directory

    def get_genres(self):
        genre_subdirectories = [x for x in os.listdir(self.directory) if not x.startswith('.')]
        return genre_subdirectories

    def get_genre_directories(self):
        return [os.path.join(self.directory, genre) for genre in self.get_genres()]

    def get_song_genre_list(self):
        song_genre_list = []
        for genre_directory in self.get_genre_directories():
            filename_list = [unicode(f, 'utf-8') for f in os.listdir(genre_directory) if not f.startswith('.')]
            filename_list = [self._compose_song_genre_tuple(filename, genre_directory) for filename in filename_list]
            song_genre_list.extend(filename_list)
        return song_genre_list

    def _compose_song_genre_tuple(self, filename, genre_directory):
        return os.path.join(genre_directory, filename), os.path.basename(genre_directory)