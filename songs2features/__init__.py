__author__ = 'dmeier'

"""
The idea of this module is to handle the conversion of physical sound files (e.g. mp3) into features and its storage
in an SQLite database, while providing methods to extract the data in the format we need for later learning. An example
would be to get a matrix X full of features and a vector y with the corresponding labels.
"""