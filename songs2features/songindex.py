import sqlite3
import json
import numpy
import random

from featurefactory import FeatureFactory
from importer import Importer
from utils import tuple2single, single2tuple, split


class Interface:
    """Master over the sqlite db that stores song data"""

    def __init__(self, sqlite_db):
        self.connection = sqlite3.connect(sqlite_db)
        self.c = self.connection.cursor()
        self.batchsize = 10
        self.train_percentage = 0.99
        self.train_ids = None
        self.test_ids = None

    def create_train_test_split(self, resplit=False):
        n = self.get_number_of_samples()
        shuffled_index = range(1, n)
        random.shuffle(shuffled_index)
        self.train_ids, self.test_ids = split(shuffled_index, self.train_percentage)

    def ensure_train_test_available(self):
        if self.train_ids is None or self.test_ids is None:
            self.create_train_test_split()

    def shuffle_training_samples(self):
        random.shuffle(self.train_ids)

    def __del__(self):
        self.connection.commit()
        self.connection.close()

    def execute(self, sql):
        self.c.execute(sql)

    def create_schema(self):
        self.c.execute("CREATE TABLE IF NOT EXISTS genre (id INTEGER PRIMARY KEY, name TEXT, UNIQUE(name))")
        self.c.execute("CREATE TABLE IF NOT EXISTS song (id INTEGER PRIMARY KEY, filename TEXT, genre_id INTEGER, features TEXT)")
        self.connection.commit()

    def insert_genre(self, genre_name):
        genre_name = (genre_name,)
        self.c.execute("INSERT OR IGNORE INTO genre (name) VALUES (?)", genre_name)
        self.connection.commit()

    def insert_genres(self, genre_names):
        genre_names = map(single2tuple, genre_names)
        self.c.executemany("INSERT OR IGNORE INTO genre (name) VALUES (?)", genre_names)
        self.connection.commit()

    def insert_song(self, filename, genre_id, features=None):
        features = json.dumps(features.tolist()) if features is not None else 'null'
        self.c.execute("INSERT INTO song VALUES(null, ?, ?, ?)", (filename, genre_id, features))
        self.connection.commit()

    def insert_song_with_genre_name(self, filename, genre_name, features=None):
        genre_id = self.get_genre_id(genre_name)
        self.insert_song(filename, genre_id, features)

    def get_genres(self):
        return map(tuple2single, list(self.c.execute("SELECT name FROM genre")))

    def get_genre_id(self, genre_name):
        genre_name = (genre_name,)
        try:
            genre_id = list(self.c.execute("SELECT id FROM genre WHERE name=?", genre_name))[0][0]
        except IndexError:
            genre_id = None
        return genre_id

    def get_feature_matrix(self):
        features = self.c.execute("SELECT features FROM song")
        features = numpy.array([json.loads(feature[0]) for feature in features])
        return features

    def get_genre_vector(self):
        genres = self.c.execute("SELECT genre_id FROM song")
        genres = numpy.array(list(genres))
        return genres

    def get_X_and_y(self):
        X = self.get_feature_matrix()
        y = self.get_genre_vector()
        return X, y

    def get_X_and_y_train_batch(self, batch_id):
        self.ensure_train_test_available()
        batch_start, batch_end = self.compute_batch_range(batch_id)
        train_batch_ids = self.train_ids[batch_start:batch_end]
        X_batch = self.get_feature_matrix_by_ids(train_batch_ids)
        y_batch = self.get_genre_vector_by_ids(train_batch_ids)
        return X_batch, y_batch

    def get_X_and_y_test(self):
        self.ensure_train_test_available()
        X = self.get_feature_matrix_by_ids(self.test_ids)
        y = self.get_genre_vector_by_ids(self.test_ids)
        return X, y

    def compute_batch_range(self, batch_id):
        start = self.batchsize*batch_id
        end = self.batchsize*(batch_id+1) - 1
        number_of_samples = self.get_number_of_samples()
        if not 0 <= start < number_of_samples:
            raise IndexError("Batch number %s (%s <= n < %s) is not available, N = %s" % (batch_id, start, end, number_of_samples))
        if not end < number_of_samples:
            end = number_of_samples-1
        return start, end

    def get_feature_matrix_one_per_song(self):
        features = self.c.execute("SELECT features FROM song WHERE id IN (SELECT id FROM song GROUP BY filename)")
        features = numpy.array([json.loads(feature[0]) for feature in features])
        return features

    def get_genre_vector_one_per_song(self):
        genres = self.c.execute("SELECT genre_id FROM song WHERE id IN (SELECT id FROM song GROUP BY filename)")
        genres = numpy.array(list(genres))
        return genres

    def get_feature_matrix_by_ids(self, ids):
        features = self.c.execute("SELECT features FROM song WHERE id IN %s" % str(tuple(ids)))
        features = numpy.array([json.loads(feature[0]) for feature in features])
        return features

    def get_genre_vector_by_ids(self, ids):
        genres = self.c.execute("SELECT genre_id FROM song WHERE id IN %s" % str(tuple(ids)))
        genres = numpy.array(list(genres))
        return genres

    def compute_max_batch_id(self):
        return int(self.get_number_of_train_samples()/self.batchsize)

    def get_feature_matrix_shape(self):
        n = self.get_number_of_samples()
        d = self.get_shape_of_a_feature()
        d = d[1] if d[0] == 1 else d
        return n, d

    def get_number_of_samples(self):
        count = [c for c in self.c.execute("SELECT count(*) FROM song")]
        return count[0][0]

    def get_number_of_train_samples(self):
        self.ensure_train_test_available()
        return len(self.train_ids)

    def get_shape_of_a_feature(self):
        example = self.c.execute("SELECT features FROM song LIMIT 1")
        example_features = numpy.array([json.loads(the_example[0]) for the_example in example])
        return example_features.shape

    def get_filenames(self):
        filenames = list(self.c.execute("SELECT filename FROM song"))
        filenames = [tuple2single(filename) for filename in filenames]
        return filenames

    def get_filenames_by_ids(self, ids):
        filenames = self.c.execute("SELECT filename FROM song WHERE id IN %s" % str(tuple(ids)))
        filenames = [tuple2single(filename) for filename in filenames]
        return filenames

    def create_songindex(self, importer, feature_factory):
        assert isinstance(importer, Importer)
        assert isinstance(feature_factory, FeatureFactory)

        genres = importer.get_genres()
        self.insert_genres(genres)
        song_genre_list = importer.get_song_genre_list()
        for idx, (song, genre) in enumerate(song_genre_list):
            self.compute_and_insert_features(song, genre, feature_factory)
            print "Added song %s/%s: %s" % (idx+1, len(song_genre_list), song)

    def compute_and_insert_features(self, song, genre, feature_factory):
        if feature_factory.many_features_available:
            features = feature_factory.get_many_features(song)
            self.insert_many_features_for_song(song, genre, features)
        else:
            features = feature_factory.get_features(song)
            self.insert_song_with_genre_name(song, genre, features)

    def insert_many_features_for_song(self, song, genre, features):
        for snippet_feature in features:
            self.insert_song_with_genre_name(song, genre, snippet_feature)
