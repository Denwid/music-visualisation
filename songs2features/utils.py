def tuple2single(tuple):
    return tuple[0]


def single2tuple(single):
    return (single,)


def split(l, percentage):
    """Split a list into two separate lists given a percentage that belongs into the first list"""
    n = len(l)
    first_element_in_second_list = int(n*percentage)
    first_part = l[0:first_element_in_second_list]
    second_part = l[first_element_in_second_list:n]
    return first_part, second_part
