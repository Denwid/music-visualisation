import random
import eyed3
import progressbar
import numpy

from utils import split


class FileTable:
    def __init__(self, directory):
        self.directory = directory
        self.file_table = []
        self.loaddir(directory)

    def loaddir(self, directory):
        self.directory = directory
        filelist = self._find_mp3_files(directory)
        self.load(filelist)

    @staticmethod
    def _find_mp3_files(directory):
        """Returns a list of every .mp3 file in the given directory or any of its subdirectories"""
        import os
        filenames = []
        for root, dirs, files in os.walk(directory):
            for file in files:
                if file.endswith('.mp3'):
                    filenames.append(os.path.join(root, file))
        return filenames

    def load(self, filenames):
        """Loads a list of dictionaries containing an entry for every track into the file_table

        Fields are:
            filename:
            nb_segments: Number of three second segments in the file
            first_segment_id:
            genre: Genre tag extracted from the MP3s ID3 tags
        """
        pb = progressbar.ProgressBar()
        eyed3.log.setLevel("ERROR")
        self.file_table = []
        print "Loading FileTable..."
        for f in pb(filenames):
            id3 = eyed3.load(f)
            row = dict()
            row["filename"] = f
            row["nb_segments"] = int(id3.info.time_secs/3)
            row["first_segment_id"] = self._next_segment_id()
            row["genre"] = id3.tag.genre.name if id3.tag.genre else None
            self.file_table.append(row)

    def _next_segment_id(self):
        if self.file_table:
            last_row = self.file_table[-1]
            return last_row["first_segment_id"]+last_row["nb_segments"]
        else:
            return 0

    def available_genres(self):
        all_genres = [row["genre"] for row in self.file_table]
        return list(set(all_genres))

    def get(self, segment_id):
        """
        :param segment_id:
        :return: the row of the FileTable that has the file with the corresponding segment
        """
        last_row = self.file_table[0]
        for row in self.file_table:
            if row["first_segment_id"] > segment_id:
                return last_row
            last_row = row
        if segment_id < self._next_segment_id():
            return last_row
        else:
            raise IndexError("There are only %s segments in %s", (self._next_segment_id(), self.directory))


class DataSource:
    """Can create X and y directly from a directory containing MP3s

    It does so by splitting each mp3 into samples of 3s. One row of X is then the feature transformation of that sample.
    The features are not calculated directly, though. Rather get_segment_features() is called with a feature ID, and the
    features are calculated on the fly.
    """

    def __init__(self, source_directory, feature_factory):
        self.file_table = FileTable(source_directory)
        self.feature_factory = feature_factory
        self.batchsize = 10
        self.train_percentage = 0.8
        self.train_ids = []
        self.test_ids = []
        self.create_train_test_split()

    def get_sample_features(self, sample_id):
        factory = self.feature_factory
        relevant_file = self.file_table.get(sample_id)
        offset = (sample_id-relevant_file["first_segment_id"])*factory.duration
        return factory.get_features(relevant_file["filename"], offset)

    def get_sample_category(self, sample_id):
        relevant_file = self.file_table.get(sample_id)
        genre_name = relevant_file["genre"]
        all_genres = self.file_table.available_genres()
        return all_genres.index(genre_name)

    def get_number_of_samples(self):
        return self.file_table.file_table[-1]["first_segment_id"] + self.file_table.file_table[-1]["nb_segments"]

    def get_shape_of_a_feature(self):
        example_features = self.get_sample_features(1)
        return example_features.shape

    def create_train_test_split(self):
        n = self.get_number_of_samples()
        shuffled_index = range(0, n-1)
        random.shuffle(shuffled_index)
        self.train_ids, self.test_ids = split(shuffled_index, self.train_percentage)

    def get_X_and_y_train_batch(self, batch_id):
        batch_start, batch_end = self.compute_batch_range(batch_id)
        train_batch_ids = self.train_ids[batch_start:batch_end]
        X_batch = self.get_feature_matrix_by_ids(train_batch_ids)
        y_batch = self.get_genre_vector_by_ids(train_batch_ids)
        return X_batch, y_batch

    def compute_batch_range(self, batch_id):
        start = self.batchsize*batch_id
        end = self.batchsize*(batch_id+1) - 1
        number_of_samples = self.get_number_of_samples()
        if not 0 <= start < number_of_samples:
            raise IndexError("Batch number %s (%s <= n < %s) is not available, N = %s" % (batch_id, start, end, number_of_samples))
        if not end < number_of_samples:
            end = number_of_samples-1
        return start, end

    def get_X_and_y_test(self):
        X = self.get_feature_matrix_by_ids(self.test_ids)
        y = self.get_genre_vector_by_ids(self.test_ids)
        return X, y

    def get_feature_matrix_by_ids(self, ids):
        return numpy.array([self.get_sample_features(i) for i in ids])

    def get_genre_vector_by_ids(self, ids):
        return numpy.array([self.get_sample_category(i) for i in ids])

    def get_feature_matrix_shape(self):
        n = self.get_number_of_samples()
        d = self.get_shape_of_a_feature()
        d = d[1] if d[0] == 1 else d
        return n, d

    def get_number_of_train_samples(self):
        return len(self.train_ids)

    def compute_max_batch_id(self):
        return int(self.get_number_of_train_samples()/self.batchsize)