import os.path
import sys

from songs2features.importer import GtzanImporter
from songs2features.featurefactory import MelSpectrumFactory
from songs2features.songindex import Interface


def yn_choice(message, default='y'):
    choices = 'Y/n' if default.lower() in ('y', 'yes') else 'y/N'
    choice = raw_input("%s (%s) " % (message, choices))
    values = ('y', 'yes', '') if default == 'y' else ('y', 'yes')
    return choice.strip().lower() in values


source_directory = sys.argv[1] if len(sys.argv) > 1 else "/Volumes/MJF/MJF1000"
database = "./spectrograms.sqlite"

print "Creating spectrograms from %s" % source_directory

if os.path.isfile(database):
    can_be_written = False
    can_be_written = yn_choice("Do you want to overwrite the existing file %s" % database)
    if can_be_written:
        os.remove(database)
else:
    can_be_written = True

if can_be_written:
    importer = GtzanImporter(source_directory)
    feature_factory = MelSpectrumFactory(offset=30)
    interface = Interface(database)

    interface.create_schema()
    interface.create_songindex(importer, feature_factory)