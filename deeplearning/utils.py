from scipy.stats import mode
import numpy


def flatten_spectrograms(X):
    if len(X.shape) == 2:
        return X

    data_size = X.shape[0]
    timesteps = X.shape[1]
    frequenzybands = X.shape[2]
    return X.reshape((data_size, timesteps*frequenzybands))


def purity(clusters, classes):
    """
    Calculate the purity score for the given cluster assignments and ground truth classes
    By Caner Turkmen, taken from http://www.caner.io/purity-in-python.html and slightly modified.

    :param clusters: the cluster assignments array (prediction)
    :type clusters: numpy.array

    :param classes: the ground truth classes (truth)
    :type classes: numpy.array

    :returns: the purity score
    :rtype: float
    """
    if type(clusters) == list:
        clusters = numpy.array(clusters)
    if type(classes) == list:
        classes = numpy.array(classes)

    A = numpy.c_[(clusters,classes)]

    n_accurate = 0.

    for j in numpy.unique(A[:,0]):
        z = A[A[:, 0] == j, 1]
        x = numpy.argmax(numpy.bincount(z))
        n_accurate += len(z[z == x])

    return n_accurate / A.shape[0]