#!/usr/bin/python

import unittest
import numpy

from utils import flatten_spectrograms, purity


class TestUtils(unittest.TestCase):
    def test_flatten_spectrograms(self):
        X = numpy.array([[[1, 1], [1, 1]], [[2, 2], [2, 2]], [[3, 3], [3, 3]]])
        reshaped_X = flatten_spectrograms(X).tolist()
        expected_X = X.reshape((3, 4)).tolist()
        self.assertEqual(reshaped_X, expected_X)

    def test_flatten_spectrograms_with_already_flat(self):
        X = numpy.array([[1, 2, 3, 4], [6, 7, 8, 9]])
        reshaped_X = flatten_spectrograms(X).tolist()
        expected_X = X.tolist()
        self.assertEqual(reshaped_X, expected_X)

    def test_purity(self):
        y_predicted = numpy.array([1, 1, 2, 2])
        y_true = numpy.array([1, 2, 2, 2])
        self.assertEqual(purity(y_predicted, y_true), 0.75)

    def test_purity2(self):
        y_predicted = numpy.array([1, 4, 4, 4, 4, 4, 3, 3, 2, 2, 3, 1, 1])
        y_true = numpy.array([5, 1, 2, 2, 2, 3, 3, 3, 1, 1, 1, 5, 2])
        self.assertEqual(purity(y_predicted, y_true), 0.6923076923076923)

    def test_purity3(self):
        y_predicted = numpy.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13])
        y_true = numpy.array([5, 1, 2, 2, 2, 3, 3, 3, 1, 1, 1, 5, 2])
        self.assertEqual(purity(y_predicted, y_true), 1)

    def test_purity_with_list(self):
        y_predicted = [1, 1, 2, 2]
        y_true = [1, 2, 2, 2]
        self.assertEqual(purity(y_predicted, y_true), 0.75)


if __name__ == '__main__':
    unittest.main()