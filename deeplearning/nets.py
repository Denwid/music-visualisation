import theano

from keras.models import Sequential
from keras.layers.core import Dense, Activation, Flatten
from keras.layers.convolutional import Convolution1D, MaxPooling1D
from keras.utils.generic_utils import Progbar
from keras.utils.np_utils import to_categorical, probas_to_classes
from keras.optimizers import SGD, Adagrad, Adadelta, Adam

from deeplearning.utils import purity


def select_optimizer(optimizer_name):
    return {
        'sgd': SGD(),
        'adagrad': Adagrad(),
        'adadelta': Adadelta(),
        'adam': Adam()
    }[optimizer_name]


class DeepNetwork:
    def __init__(self, model=None):
        if model:
            self.model = model
        else:
            self.model = self.define_configuration()
        self.outputs = []

    def define_configuration(self):
        # define self.model
        return None

    def compute_layer_outputs(self, X):
        for i in range(0, len(self.model.layers)):
            f = theano.function([self.model.layers[0].input], self.model.layers[i].get_output(train=False))
            self.outputs.append(f(X))
            print "Computed layer %i response. Output format: %s" % (i, self.outputs[i].shape)

    def get_layer_output(self, layer_idx, X=None, recalculate=False):
        # e.g.
        # Layer 0 output format: (1000, 769, 32)
        # Layer 1 output format: (1000, 192, 32)
        # Layer 2 output format: (1000, 185, 32)
        # Layer 3 output format: (1000, 46, 32)
        # Layer 4 output format: (1000, 1472)
        # Layer 5 output format: (1000, 100)
        # Layer 6 output format: (1000, 100)
        # Layer 7 output format: (1000, 11)
        if X is not None and recalculate:
            self.compute_layer_outputs(X)
        return self.outputs[layer_idx]

    def get_classification_output(self, X=None):
        return self.get_layer_output(-1, X)

    def train_batchwise(self, songindex_interface):
        nb_total_samples = songindex_interface.get_number_of_train_samples()
        songindex_interface.shuffle_training_samples()
        progressbar = Progbar(nb_total_samples)
        b_max = songindex_interface.compute_max_batch_id()
        losses = []
        for b in range(0,b_max):
            X, y = songindex_interface.get_X_and_y_train_batch(b)
            try:
                loss = self.train_on_batch(X, y)
            except TypeError as e:
                print e.message
                print "X and y were of size %s and %s" % (str(X.shape),str(y.shape))
            losses.append(loss)
            display_list = [
                ("batch train loss", loss),
                ("avg train loss", sum(losses)/float(len(losses)))
            ]
            progressbar.update(songindex_interface.batchsize*(b+1), values=display_list)

    def get_layer_tsne_output(self, layer_id, n_components=2):
        from sklearn.manifold import TSNE
        from utils import flatten_spectrograms
        model = TSNE(n_components=n_components, random_state=0)
        input = flatten_spectrograms(self.outputs[layer_id])
        print "Compute layer %s TSNE, %s -> %sD points " % (layer_id, self.outputs[layer_id].shape, n_components)
        print "Example input: %s" % input[0]
        tsne_output = model.fit_transform(input)
        return tsne_output

    def train_on_batch(self, X_train, y_train):
        assert isinstance(self.model, Sequential)
        y_train = to_categorical(y_train, nb_classes=self.output_layer_size)
        loss = self.model.train_on_batch(X_train, y_train)
        return loss

    def compute_prediction_purity(self, X_test, y_test):
        y_predicted = probas_to_classes(self.model.predict(X_test))
        return purity(y_predicted, y_test)


# Eck
# implementing the network described in eck10.pdf
class FourLayerDBN(DeepNetwork):
    def __init__(self, input_layer_shape):
        self.output_layer_size = 11
        self.model = self.define_configuration(input_layer_shape)
        self.outputs = []

    def define_configuration(self, input_dim=500):
        model = Sequential()
        model.add(Dense(100, input_dim=input_dim))
        model.add(Activation("sigmoid"))
        model.add(Dense(50))
        model.add(Activation('sigmoid'))
        model.add(Dense(50))
        model.add(Activation('sigmoid'))
        model.add(Dense(self.output_layer_size))
        model.add(Activation('sigmoid'))

        model.compile(loss='categorical_crossentropy', optimizer='sgd')
        return model


# Dmytro
class Dmytro(DeepNetwork):
    def __init__(self, input_layer_shape):
        self.output_layer_size = 11
        self.model = self.define_configuration(input_layer_shape)
        self.outputs = []

    def define_configuration(self, input_dim=500):
        model = Sequential()
        model.add(Dense(100, input_dim=input_dim))
        model.add(Activation("sigmoid"))
        model.add(Dense(100))
        model.add(Activation('sigmoid'))
        model.add(Dense(self.output_layer_size))
        model.add(Activation('sigmoid'))

        model.compile(loss='categorical_crossentropy', optimizer='sgd')
        return model


# Dieleman
class SixLayerConvolutional(DeepNetwork):
    def __init__(self, input_layer_shape, output_layer_size=11, optimizer_name='sgd'):
        self.output_layer_size = output_layer_size
        self.model = self.define_configuration(input_layer_shape, 11, optimizer_name)
        self.outputs = []

    def define_configuration(self, input_layer_shape, output_layer_size, optimizer_name):
        model = Sequential()
        model.add(Convolution1D(32, 8, activation='relu', input_shape=input_layer_shape))
        model.add(MaxPooling1D(pool_length=4))
        model.add(Convolution1D(32, 8, activation='relu'))
        model.add(MaxPooling1D(pool_length=4))
        model.add(Flatten())
        model.add(Dense(100))
        model.add(Activation("relu"))
        model.add(Dense(self.output_layer_size))
        model.add(Activation("softmax"))
        optimizer = select_optimizer(optimizer_name)
        model.compile(loss='categorical_crossentropy', optimizer=optimizer)
        return model


# Lee
class TwoLayerConvolutional(DeepNetwork):
    def train(self, X_train, y_train):
        y_train = to_categorical(y_train)

        model = Sequential()
        model.add(Convolution1D(300, 6, activation='relu', input_shape=X_train.shape[1:3]))
        model.add(MaxPooling1D(pool_length=3))
        model.add(Convolution1D(300, 6, activation='relu', input_shape=X_train.shape[1:3]))
        model.add(MaxPooling1D(pool_length=3))
        model.add(Flatten())
        model.add(Dense(y_train.shape[1]))
        model.add(Activation("softmax"))


        model.compile(loss='categorical_crossentropy', optimizer='sgd')
        model.fit(X_train, y_train, nb_epoch=10, batch_size=30, verbose=1, show_accuracy=True)

        self.model = model
        self.X_train = X_train
        self.y_train = y_train