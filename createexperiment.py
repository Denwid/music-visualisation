import sys


from visualize.experiment import (create_experiment, train_and_create_experiment, load_model_from_experiment_dir,
                                  compose_experiment_name, compose_cross_name)
from deeplearning.nets import DeepNetwork
from songs2features import songindex

if len(sys.argv) == 2:
    existing_experiment = sys.argv[1]
else:
    existing_experiment = None;

gtzan = "/Users/dmeier/Documents/Datasets/GTZANMelSpectrum3.sqlite"
mjf = "/Users/dmeier/Documents/Datasets/MJF1000MelSpec.sqlite"
mjf_normalized = "/Users/dmeier/Documents/Datasets/MJF1000MelSpecNormalized.sqlite"
mjf304 = "/Users/dmeier/Documents/Datasets/MJF304MelSpecNormalized.sqlite"

#configuration
source = "./spectrograms.sqlite"
second_source = None;
nb_epochs = 20
optimizer = 'adagrad'
train_percentage = 0.99



if existing_experiment:
    model = load_model_from_experiment_dir(existing_experiment)
    network = DeepNetwork(model)


    print "Creating %s..." % compose_cross_name(source, existing_experiment)

    data_source = songindex.Interface(source)
    data_source.ensure_train_test_available()

    filenames = data_source.get_filenames()
    print "Loading X..."
    X = data_source.get_feature_matrix().tolist()
    print "Loading y..."
    y = data_source.get_genre_vector().tolist()

    create_experiment(network, filenames, X, y)
else:
    print "Creating %s..." % compose_experiment_name(source, nb_epochs, optimizer, train_percentage)
    train_and_create_experiment(source, nb_epochs, optimizer, train_percentage)
