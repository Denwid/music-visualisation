import sys
import os

from visualize.visualize2 import reduce_and_visualize
from visualize.visualize3 import visualize3d_layer
from visualize.experiment import load_json_from_experiment, reduce_experiment_json_by_filename, chose_experiment_name


if len(sys.argv) > 1:
    experiment_name = sys.argv[1]
else:
    experiment_name = chose_experiment_name()

experiment = load_json_from_experiment(experiment_name)
if "3d" in experiment_name:
    reduced_experiment = reduce_experiment_json_by_filename(experiment)
    ax = visualize3d_layer(reduced_experiment, experiment_name, 7)
else:
    reduce_and_visualize(experiment, experiment_name, no_reduction=False)
