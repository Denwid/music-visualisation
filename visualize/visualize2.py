import json
from bokeh.plotting import figure, output_file, ColumnDataSource, gridplot, save
from bokeh.models import HoverTool, OpenURL, TapTool

from experiment import reduce_experiment_json_by_filename


DColors = [(240,163,255),(0,117,220),(153,63,0),(76,0,92),(25,25,25),(0,92,49),(43,206,72),(255,204,153),
           (128,128,128),(148,255,181),(143,124,0),(157,204,0),(194,0,136),(0,51,128),(255,164,5),(255,168,187),
           (66,102,0),(255,0,16),(94,241,242),(0,153,143),(224,255,102),(116,10,255),(153,0,0),(255,255,128),
           (255,255,0),(255,80,5)]
DColors2 = [(166,206,227), (31,120,180), (178,223,138), (51,160,44), (251,154,153), (227,26,28), (253,191,111),
            (255,127,0), (202,178,214), (106,61,154)]
DColors3 = [(141,211,199), (230,230,179), (190,186,218), (251,128,114), (128,177,211), (253,180,98), (179,222,105),
            (252,205,229), (217,217,217), (188,128,189)]
DColors2Named = {
    "reggae": (51,160,44),
    "Reggae": (51,160,44),
    "classical": (166,206,227),
    "country": (160,82,45),
    "Folk": (160,82,45),
    "jazz": (106,61,154),
    "Jazz": (106,61,154),
    "metal": (253,191,111),
    "pop": (227,26,28),
    "disco": (214,214,0),
    "hiphop": (0,0,0),
    "Hip-Hop": (0,0,0),
    "rock": (144,144,144),
    "blues": (31,120,180),
    "Blues": (31,120,180),
    "Soul": (166,206,227),
    "Alternative": (251,154,153),
    "Electronic": (255,127,0),
}


def create_data_sources(experiment, layer_index):
    series = {}
    for idx, c in enumerate(experiment["xy"][layer_index]):
        label = str(experiment["genre_names"][idx])
        try:
            series[label]["x"].append(c[0])
            series[label]["y"].append(c[1])
            series[label]["title"].append(experiment["titles"][idx])
            series[label]["artist"].append(experiment["artists"][idx])
            series[label]["filename"].append(experiment["filenames"][idx])
        except KeyError:
            series[label] = {}
            series[label]["x"] = [c[0]]
            series[label]["y"] = [c[1]]
            series[label]["title"] = [experiment["titles"][idx]]
            series[label]["artist"] = [experiment["artists"][idx]]
            series[label]["filename"] = [experiment["filenames"][idx]]

    sources = {}
    for label in series.iterkeys():
        sources[label] = ColumnDataSource(series[label])

    return sources

def reduce_and_visualize(experiment, experiment_name, no_reduction=False):
    if no_reduction:
        reduced_experiment = experiment
    else:
        reduced_experiment = reduce_experiment_json_by_filename(experiment)
    visualize(reduced_experiment, experiment_name)

def visualize(experiment, experiment_name="Experiment visualization"):
    output_file("markers.html")
    plot_list = []
    nb_layers = len(experiment["xy"])
    for layer_id in reversed(range(0, nb_layers)):
        title = "Layer %s" % layer_id
        plot_list.append(figure(title=title, tools="reset,hover,tap,box_zoom,pan,wheel_zoom"))
        sources = create_data_sources(experiment, layer_id)
        idx = 0
        for label, source in sources.iteritems():
            plot_list[-1].scatter('x', 'y', size=8, source=source, legend=label, color=DColors2Named[label])
            plot_list[-1].select(dict(type=HoverTool)).tooltips = {"Artist":"@artist", "Title":"@title"}
            plot_list[-1].select(type=TapTool).callback = OpenURL(url="file://@filename")

            idx += 1

    p = gridplot([[plot] for plot in plot_list], toolbar_location=None)
    experiment_name = "experiment" if experiment_name is "" else experiment_name
    save(p, filename=experiment_name+".html", title=experiment_name)
