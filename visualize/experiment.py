import json
import os

from keras.optimizers import SGD
from keras.models import model_from_json


from songs2features import songindex
from deeplearning import nets
from utils import filename2artist, filename2genrename, filename2title, geometric_mean

experiments_library_path = "../experiments"

def train_network(interface, decrease_linearily=False, nb_epochs=40, optimizer_name='sgd'):
    print "Loading data..."
    X_, y_ = interface.get_X_and_y_test()
    input_shape = X_.shape[1:3]
    print "Training the network..."
    nn = nets.SixLayerConvolutional(input_shape, optimizer_name=optimizer_name)
    print "Running %s epochs" % nb_epochs
    for epoch in xrange(0,nb_epochs):
        if isinstance(nn.model.optimizer, SGD) and decrease_linearily:
            base_learning_rate = 0.1
            dynamic_learning_rate = float((nb_epochs-epoch))/75*base_learning_rate
            nn.model.optimizer.lr.set_value(dynamic_learning_rate)
            print "Learning rate set to %s" % dynamic_learning_rate
        nn.train_batchwise(interface)
        test_purity = nn.compute_prediction_purity(X_, y_)
        print "\n Epoch %s finished. Test purity %s." % (epoch, test_purity)

    return nn


def get_layer_tsne_outputs(network, X, n_components=2):
    assert isinstance(network, nets.DeepNetwork)
    layer_outputs = []
    network.compute_layer_outputs(X)
    for i in xrange(0, len(network.model.layers)):
        layer_outputs.append(network.get_layer_tsne_output(i, n_components=n_components).tolist())
    return layer_outputs


def get_classification_output(network, X):
    output = network.get_classification_output(X)
    return output.tolist()


def train_and_create_experiment(source, nb_epochs, optimizer_name='sgd', train_percentage='0.99'):
    # experiment with the training
    data_source = songindex.Interface(source)
    data_source.train_percentage = train_percentage
    data_source.ensure_train_test_available()
    network = train_network(data_source, nb_epochs=nb_epochs, optimizer_name=optimizer_name)
    print "Finished with training, visualizing and saving results..."
    ids_to_visualize = data_source.train_ids
    filenames = data_source.get_filenames_by_ids(ids_to_visualize)
    X = data_source.get_feature_matrix_by_ids(ids_to_visualize)
    y = data_source.get_genre_vector_by_ids(ids_to_visualize).tolist()
    create_experiment(network, filenames, X, y)


def create_experiment(network, filenames, X, y, n_tsne_components=2):
    experiment = {}
    experiment["filenames"] = filenames
    print "Saved filenames..."
    experiment["genre_ids"] = y
    print "Saved genre_ids..."
    experiment["genre_names"] = map(filename2genrename, experiment["filenames"])
    print "Saved genre_names..."
    experiment["titles"] = map(filename2title, experiment["filenames"])
    print "Saved titles..."
    experiment["artists"] = map(filename2artist, experiment["filenames"])
    print "Saved artists..."
    experiment["xy"] = get_layer_tsne_outputs(network, X, n_tsne_components)
    print "Saved xy..."
    experiment["classification"] = get_classification_output(network, X)
    print "Saved classification scores..."

    save_experiment(experiment, network)



def save_experiment(experiment_results, network):
    with open("experiment.json", "w") as fp:
        json.dump(experiment_results, fp)
    with open("experiment_network_architecture.json", 'w') as fp:
        json.dump(network.model.to_json(), fp)
    network.model.save_weights('experiment_network_weights.h5')


def load_model_from_experiment_dir(experiment_name):
    experiment = name2experiment(experiment_name)
    print "Loading model %s..." % experiment_name
    architecture = os.path.join(experiment, "experiment_network_architecture.json")
    architecture = json.load(open(architecture, "r"))
    model = model_from_json(architecture)
    weights = os.path.join(experiment, "experiment_network_weights.h5")
    model.load_weights(weights)


def load_json_from_experiment(experiment_name):
    experiment = name2experiment(experiment_name)
    experiment_json_path = os.path.join(experiment, "experiment.json")
    return json.load(open(experiment_json_path, "r"))


def name2experiment(experiment_name=""):
    library_path = experiments_library_path
    if os.path.isdir(library_path) and experiment_name != "":
        experiment = os.path.join("../experiments", experiment_name)
    elif not os.path.isdir(library_path) and experiment_name != "":
        experiment = None
        raise IOError("No library found to load experiment %s: %s doesn't exist!" % (experiment_name, experiments_library_path))
    else:
        experiment = "./"
    return experiment


def reduce_experiment_json_by_filename(experiment):
    print "Reducing experiment by filename."
    file_data = {}
    n_original = len(experiment["filenames"])
    for idx, f in enumerate(experiment["filenames"]):
        try:
            for i in range(0,len(experiment["xy"])):
                file_data[f]["coord_list"][i].append(experiment["xy"][i][idx])
        except KeyError:
            file_data[f] = {}
            file_data[f]["coord_list"] = []
            for i in range(0, len(experiment["xy"])):
                file_data[f]["coord_list"].append([experiment["xy"][i][idx]])
            file_data[f]["title"] = experiment["titles"][idx]
            file_data[f]["artist"] = experiment["artists"][idx]
            file_data[f]["genre_name"] = experiment["genre_names"][idx]
            file_data[f]["genre_id"] = experiment["genre_ids"][idx]

    reduced_experiment = {"xy": [[] for i in range(0, len(experiment["xy"]))],
                          "filenames": [],
                          "titles": [],
                          "artists": [],
                          "genre_names": [],
                          "genre_ids": []
                          }
    for f, data in file_data.iteritems():
        for i in range(0,len(experiment["xy"])):
            reduced_experiment["xy"][i].append(geometric_mean(data["coord_list"][i]))
        reduced_experiment["titles"].append(data["title"])
        reduced_experiment["artists"].append(data["artist"])
        reduced_experiment["filenames"].append(f)
        reduced_experiment["genre_names"].append(data["genre_name"])
        reduced_experiment["genre_ids"].append((data["genre_id"]))

    n_reduced = len(reduced_experiment["filenames"])
    print "Reduced from %s to %s datapoints" % (n_original, n_reduced)

    return reduced_experiment


def extract_source_name_from_source(source):
    return ".".join(os.path.basename(source).split(".")[0:-1])


def compose_experiment_name(source, nb_epochs, optimizer_name, train_percentage):
    source_name = extract_source_name_from_source(source)
    train_percentage_string = str(int(train_percentage*100))
    return "%s-%sepochs-%s-%straining" % (source_name, nb_epochs, optimizer_name, train_percentage_string)


def compose_cross_name(source, experiment_name):
    source_name = extract_source_name_from_source(source)
    return "%s-VS-%s" % (source_name, experiment_name)


def chose_experiment_name():
    default_experiment = "gtzan-dieleman-160epochs-adagrad-99training-3d"
    if os.path.isdir(experiments_library_path):
        experiment_list = os.listdir(experiments_library_path)
        for idx, e in enumerate(experiment_list):
            print "%s: %s" % (idx, e)
        try:
            choice = int(input("Chose which experiment to visualize: "))
            experiment_name = experiment_list[choice]
            print "Visualizing %s" % experiment_name
        except ValueError:
            print "No int chosen, visualizing default experiment: %s" % default_experiment
            experiment_name = default_experiment
    else:
        print "No experiments library available, using simply the working directory."
        experiment_name = ""
    return experiment_name