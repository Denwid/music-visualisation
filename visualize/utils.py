import os


def filename2genrename(filename):
    return os.path.dirname(filename).split("/")[-1]


def filename2title(filename):
    if " -x- " in filename:
        return filename.split(" -x- ")[1]
    else:
        return os.path.basename(filename)


def filename2artist(filename):
    if " -x- " in filename:
        return filename.split(" -x- ")[0].split("/")[-1]
    else:
        return None


def geometric_mean(list_of_coords):
    nb_coords = float(len(list_of_coords))
    geometric_mean = [sum(c)/nb_coords for c in zip(*list_of_coords)]
    return tuple(geometric_mean)
