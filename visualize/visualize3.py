import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D # needed for add_subplot(..., projection='3d')
import numpy

from experiment import reduce_experiment_json_by_filename

def create_series(experiment, layer_index):
    xs = []
    ys = []
    zs = []
    for idx, coords in enumerate(experiment["xy"][layer_index]):
        label = str(experiment["genre_names"][idx])
        xs.append(coords[0])
        ys.append(coords[1])
        zs.append(coords[2])

    return numpy.array(xs), numpy.array(ys), numpy.array(zs)

def create_classes(experiment):
    return [id[0] for id in experiment["genre_ids"]]

def visualize3d(experiment, experiment_name="Experiment visualization"):
    nb_layers = len(experiment["xy"])
    fig = plt.figure()
    plt.title(experiment_name)
    nrows = 1
    ncols=3
    for layer_id in range(6, nb_layers):
        xyz = create_series(experiment, layer_id)
        classes = create_classes(experiment)
        ax = fig.add_subplot(nrows, ncols, layer_id-6, projection='3d')
        ax.scatter(xyz[0], xyz[1], xyz[2], c=classes)

    plt.show()

def reduce_and_visualize3d(experiment, experiment_name):
    reduced_experiment = reduce_experiment_json_by_filename(experiment)
    visualize3d(reduced_experiment, experiment_name)

def visualize3d_layer(experiment, experiment_name, layer_id):
    fig = plt.figure()
    xyz = create_series(experiment, layer_id)
    classes = create_classes(experiment)
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(xyz[0], xyz[1], xyz[2], c=classes)
    ax.set_title("%s Layer %s" % (experiment_name, layer_id))
    plt.show()
    return ax

def visualize3d_highest_layer(experiment, experiment_name):
    nb_layers = len(experiment["xy"])
    visualize3d_layer(experiment, experiment_name, nb_layers)

