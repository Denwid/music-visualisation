#!/usr/bin/python

import unittest
from utils import filename2artist, filename2genrename, filename2title, geometric_mean


class TestUtils(unittest.TestCase):
    def setUp(self):
        self.f1 = "/Users/dmeier/Music/gtzan-genres-dataset/blues/blues.00075.au"
        self.f2 = "/Volumes/MJF/MJF1000/Reggae/The Abyssinians -x- Abendigo -x- The Montreux Jazz Festival Archive -x- 2000 -x- 11 -x- 46999.mp3"

    def test_filename2artist(self):
        self.assertEqual(filename2artist(self.f1), None)
        self.assertEqual(filename2artist(self.f2), "The Abyssinians")

    def test_filename2genrename(self):
        self.assertEqual(filename2genrename(self.f1), "blues")
        self.assertEqual(filename2genrename(self.f2), "Reggae")

    def test_filename2title(self):
        self.assertEqual(filename2title(self.f1), "blues.00075.au")
        self.assertEqual(filename2title(self.f2), "Abendigo")

    def test_geometric_mean_2d(self):
        four_points_around_zero = [(1, 0), (0, -1), (-1, 0), (0, 1)]
        two_points = [(1, 0), (0, 1)]
        self.assertEqual(geometric_mean(four_points_around_zero), (0, 0))
        self.assertEqual(geometric_mean(two_points), (0.5, 0.5))

    def test_geometric_mean_3d(self):
        six_points_around_zero = [(1, 0, 0), (0, 1, 0), (0, 0, 1),
                                  (-1, 0, 0), (0, -1, 0), (0, 0, -1)]
        self.assertEqual(geometric_mean(six_points_around_zero), (0, 0, 0))


if __name__ == '__main__':
    unittest.main()