Visualizing Hidden Structures in the Montreux Jazz Festival Archive
===================================================================

What is this?
-------------
This is code for preprocessing and visualizing GTZAN and MJF1000 music archives
using spectrograms, deep learning and t-SNE. It was written for my Fall 2015
semester project at EPFL (www.epfl.ch).

Three user-scripts are provided:

* createsqlite.py, creates a sqlite database of tracks and their spectrograms.
* createexperiment.py, trains a network and creates an experiment.json file
  which contains t-SNE outputs for all layers along with info on the tracks
  that belong to these points.
* createvisualization.py, given a previously created experiment.json file
  this creates a corresponding visualization of all the layers.

These scripts use functions from the three modules songs2features, deeplearning
and visualize that contain the implementation details.

A demo iPython notebook can be found in demo.ipynb . You need GTZAN or an
MJF1000 dataset to run this code on.

This project is in Python (the predecessor was in Matlab). More specifically:
The spectrogram extraction and audio manipulation was done with librosa. The
Keras framework  was used for the deep learning implementation. The bokeh
visualization library is used for visualizing. The complete requirements
with versions are given in requirements.txt (as usual in Python projects).

https://github.com/bmcfee/librosa
http://keras.io/
http://bokeh.pydata.org/en/latest/


Related resources & contact
---------------------------
The scientific report going with this code can be found on the project progress
blog in the last post: https://lts2.epfl.ch/blog/dennis/. Most of the contents
of the blog have been summarized in the report, but the blog itself might still
contain some details that are not anywhere else.

The source is available on https://bitbucket.org/Denwid/music-visualisation/src

Dennis Meier, dennis.meier@epfl.ch or denwid@gmx.ch


Acknowledgements
----------------
Supervised by Xavier Bresson (http://people.epfl.ch/140163) and Kirell Benzi
(http://people.epfl.ch/kirell.benzi), Signal Processing Laboratory 2 of EPFL.
This was written on the shoulders of many an open source software writing
volunteer, let them be praised.
